
const database = require('../config/config.js');

const express = require('express')

const router = express.Router()

//account registration
router.post('/account/registration',(req,res)=>{
 
			let responBack = { 	respon : "Failed",isSuccess : false};
			
						let ref = database.ref("Account/"+req.body.username);
						ref.once('value').then(function(snapshot) {
								if(snapshot.val()!= null){
									responBack.respon = "Username Already Exist";
								}else {
									
									ref.set({ auth : {pass : req.body.pass, token : ""},
												udata :
												{name : req.body.name ,
												username : req.body.username, email : req.body.email , 
												pass : req.body.pass ,phone : req.body.phone, credit : 0}
												});
									responBack.isSuccess = true;
									responBack.respon = "User created successfully";
								}
								res.send(responBack);
						});
							
})

//login
router.post('/account/login' , (req , res) => {
			
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	token:"",username:"",respon : "Failed",isSuccess : false};
			let tok = "";
			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						if(snapshot.val().auth.pass!=req.body.pass){
							responBack.respon = "Login Failed, Password doesn't match";
						}else{
							tok=token();
							
							ref.child('auth').update({token: tok});
							responBack.respon = "Login Success";
							responBack.isSuccess = true;
							responBack.token = tok;
							responBack.username = snapshot.val().udata.username;
						}
					}else {
						responBack.respon = "Login Failed, username doesn't exist";
					}
					res.send(responBack);
					responBack = null;
			});
			
})


//update account
router.post('/account/update' , (req , res) => {
			
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	respon : "Failed",isSuccess : false};
	
			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						if(snapshot.val().auth.token!="" && snapshot.val().auth.token == req.body.token && snapshot.val().auth.pass==req.body.pass){
							ref.child('udata').update({name : req.body.name ,
										email : req.body.email ,
										phone : req.body.phone}
							);
							responBack.isSuccess = true;
							responBack.respon = "User Updated successfully";
						}
					}else {
						responBack.respon = "User Update Error";
					}
					res.send(responBack);
					responBack = null;
			}).catch(function(err){
								responBack.respon = ref;
								res.send(responBack);
							});	
})




router.post('/account/updatepass' , (req , res) => {
			
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	respon : "Failed",isSuccess : false};
	
			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						console.log(snapshot.val().auth.pass);
						console.log(req.body.oldpass);
						if(snapshot.val().auth.token!="" && snapshot.val().auth.token == req.body.token && snapshot.val().auth.pass == req.body.oldpass){
							ref.child('udata').update({pass : req.body.newpass});
							ref.child('auth').update({pass : req.body.newpass});
							responBack.isSuccess = true;
							responBack.respon = "Password Changed successfully";
						}
					}else {
						responBack.respon = "Password Changing Error";
					}
					res.send(responBack);
					responBack = null;
			});
			
})
//logout 
router.post('/account/logout' , (req , res) => {
			 
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	respon : "Failed",isSuccess : false};

			ref.once('value').then(function(snapshot) {
					if(snapshot.val().auth.token != ""){
						ref.child('auth').update({token :""});
						responBack.isSuccess = true;
						responBack.respon = "Logout Success";
					}else {
						responBack.respon = "Logout Failed, Account not loged in";
					}
					res.send(responBack);
					responBack = null;
			});
			
})

//get account
router.post('/account/get' , (req , res) => {
			
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	respon : "Failed",isSuccess : false,
			phone :"", name:"",email:"",credit:0};

			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						if(snapshot.val().auth.token!="" && snapshot.val().auth.token == req.body.token){
							responBack.respon = "Success";
							responBack.isSuccess = true;
							responBack.phone = snapshot.val().udata.phone;
							responBack.name = snapshot.val().udata.name;
							responBack.email = snapshot.val().udata.email;
							responBack.credit = snapshot.val().udata.credit;
						}
					}else {
						responBack.respon = "Getting account Failed, username doesn't exist";
					}
					res.send(responBack);
			});
			
})


//get ticket
router.post('/account/ticket' , (req , res) => {
			
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	respon : "Failed",isSuccess : false };

			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						if(snapshot.val().auth.token!="" && snapshot.val().auth.token == req.body.token){

							let ref2 = database.ref("Ticket");
							ref2.orderByChild("uid").equalTo(req.body.username).once('value').then(function(snapshot) {
								responBack.respon = "Data loaded successfully";
								responBack.isSuccess = true;
								responBack.data = snapshot;
								res.send(responBack);
							}).catch(function(err){
								responBack.respon = "can't get data";
								res.send(responBack);
							});
							
						}else{
							responBack.respon = "Can't Auth user";
							res.send(responBack);
						}
					}else {
						responBack.respon = "Getting account Failed, username doesn't exist";
						res.send(responBack);
					}
					
			});
					
})

//get payment orders
router.post('/account/orders' , (req , res) => {
			
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	respon : "Failed",isSuccess : false };

			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						if(snapshot.val().auth.token!="" && snapshot.val().auth.token == req.body.token){

							let ref2 = database.ref("History");
							ref2.orderByChild("uid").equalTo(req.body.username).once('value').then(function(snapshot) {
								responBack.respon = "Data loaded successfully";
								responBack.isSuccess = true;
								responBack.data = snapshot;
								res.send(responBack);
							}).catch(function(err){
								responBack.respon = "can't get data";
								res.send(responBack);
							});
							
						}else{
							responBack.respon = "Can't Auth user";
							res.send(responBack);
						}
					}else {
						responBack.respon = "Getting account Failed, username doesn't exist";
						res.send(responBack);
					}
					
			});
					
})

//get payment orders
router.post('/account/cancelorders' , (req , res) => {
			
			let ref = database.ref("Account/"+req.body.username);
			let responBack = { 	respon : "Failed",isSuccess : false };

			ref.once('value').then(function(snapshot) {
					if(snapshot.val() != null){
						if(snapshot.val().auth.token!="" && snapshot.val().auth.token == req.body.token){

							let ref2 = database.ref("History/"+req.body.key);
							responBack.isSuccess = true;
							ref2.update({tstatus : 0});
							responBack.respon = "Payment canceled successfully";
							res.send(responBack);
						}else{
							responBack.respon = "Can't Auth user";
							res.send(responBack);
						}
					}else {
						responBack.respon = "Getting account Failed, username doesn't exist";
						res.send(responBack);
					}
					
			});
					
})


//token validator
var rand = function() {
    return Math.random().toString(36).substr(2); // remove `0.`
};

var token = function() {
    return rand() + rand(); // to make it longer
};

module.exports = router;